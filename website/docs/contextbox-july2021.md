---
id: contextbox-july2021
title: "LINCS Context Box: Jul. 12 2021"
---
### Frolic Focus
This Design Frolic was centred around the [LINCS Context Box](https://gitlab.com/calincs/access/info-box-widget), (also known as the LINCS Info Box Widget), which is currently in development. The Frolic was split into two sections: in the first, participants were asked to use Figma to  envision themselves in a research scenario, and create information maps of a given entity, brainstorming and visually mapping what information would be most interesting, relevant, and give context. In the second section, groups took those information maps and used them to help inform their designs for a pop-up that would appear with information about that entity on a webpage, again within that hypothetical research scenario. 

### Participants
Organized by Rashmeet Kaur, Kathleen McCulloch-Cop, and Kim Martin, with support from Dawson MacPhee. 

#### Group 1
- Gracy Go
- Alliya Mo
- Hannah Stewart

#### Group 2:
- Susan Brown
- Mihaela Ilovan
- Kim Martin

#### Group 3:
- Sana Javeed
- Sarah Mousseau
- Sam Peacock

#### Group 4
- Robin Bergart
- Amardeep Singh
- Sarah Roger

### Designs

#### Group 1:
![Group 1 Design](/img/july2021_gr1.png)
#### Group 2:
![Group 2 Design (Pop-Up)](/img/july2021_gr2a.png)
![Group 2 Design (Timeline)](/img/july2021_gr2b.png)
#### Group 3:
![Group 3 Design](/img/july2021_gr3.png)
#### Group 4:
![Group 4 Design](/img/july2021_gr4.png)
