---
title: Past + Future Frolics
slug: /
---

## Interested in Frolicking?
Keep an eye on these pages for details about past and upcoming design frolics!

## Upcoming:
- Early June 2022: LINCS ResearchSpace Home Pages and Project Pages

### Contact us
Want to participate? Got a design problem you want to have frolic about?
You can get in touch with Kim Martin through [her website](https://www.kim-martin.ca/)
