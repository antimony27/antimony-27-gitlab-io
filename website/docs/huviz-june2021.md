---
id: huviz-june2021
title: "HuViz: Jun. 23 2021"
slug: /huviz-june2021
---

### Frolic Focus

As part of the inaugural design frolic, we've invited students + folks from LINCS to work together, get creative, and solve some design-related problems with [HuViz](http://huviz.lincsproject.ca/), a network visualization interface for Linked Open Data.

We asked participants to imagine a version of [HuViz](http://huviz.lincsproject.ca/) where the components and functionality that currently reside in the commands panel were broken up, and distributed across the interface.


### Participants
Organized by Rashmeet Kaur, Kathleen McCulloch-Cop, and Kim Martin.

#### Group 1
- Gracy Go
- Alliya Mo
- Sam Peacock
- Hannah Stewart

#### Group 2:
- Kelly Hughes
- Sana Javeed
- Sarah Mousseau
- Amardeep Singh
- Thomas Smith



#### Group 3:
- Robin Bergart
- Devon Hayley Farrell
- Dawson MacPhee
- Sarah Roger

### Designs

#### Group 1:
![Group 1 Design](/img/june2021_gr1.png)
#### Group 2:
![Group 2 Design](/img/june2021_gr2.png)
#### Group 3:
![Group 3 Design](/img/june2021_gr3.png)
