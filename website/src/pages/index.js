import React from 'react';
import Layout from '@theme/Layout';
import clsx from 'clsx';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';

const features = [
  {
    description: <>From the Amish tradition, a frolic is traditionally a large gathering with the aim to solve a problem, foster community, and enjoy yourselves.
    Design frolics are just that: a chance to collaborate, explore, play, and get creative.
    </>
  },
  {
    description: (<>
      Each is focused around a specific prompt: a design problem or concept that could benefit from brainstorming and collaborative idea generation.
      </>
    ),
  },
  {
    description:
      <>
      This is a time to have fun, explore, try out new things, and work with others to try to solve some design problems.
      There's no pressure, no competition, and no expectation; just an opportunity for people to play around and try to design something new.
      </>
  },
];

const frolics = [
  {
    date: <>July 12,2021</>,
    name: "LINCS Context Box",
    link: "/docs/contextbox-july2021",
    colourcode: <>#9C7DB8</>,
    background: '#105955',
    linkdec: "underline",
  },
  {
    date: <>June 23, 2021</>,
    name: <>HuViz</>,
    link: "/docs/huviz-june2021",
    colourcode: <>#AE8CB8</>,
    background: "#206e68",
    linkdec: "underline",
  },
  {
    date: <>Early June, 2022</>,
    name: <>LINCS ResearchSpace Home+Project Pages</>,
    colourcode: <>#DEB2EB</>,
    background: '#2d827a',
  },
  {
    name: <>Check back for more upcoming frolics!</>,
    colourcode: <>#DDCDEB</>,
    background: '#388980',
  }
];

function Frolic({name, date, colourcode, link, background, linkdec}) {
  const url = useBaseUrl(link);
  return (
    <div className={clsx('col ', 'paintchip')} style={{color:"#ffffff", background: background,
    margin: "10px 0px 10px 5px"}}>
      <p><a href={url} style={{color:"#ffffff", "text-decoration": linkdec}}>{name}</a></p>
      <div class="colourcode">
        <span style={{'font-size':"1vw"}}>{date}</span>
        <span>{colourcode}</span>
      </div>
    </div>
  );
}

function Feature({description}) {
  return (
    <div className={clsx('col col--4')}>
      <p>{description}</p>
    </div>
  );
}

export default function Home(){
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout>
      <div>
      <div class="logo">
        <img src="./img/designfrolicLogo.jpg"/>
      </div>
        {features && features.length > 0 && (
          <section>
            <div className="container">

              <h1 class="label">What's a Design Frolic?</h1>
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
            </section>
        )}
        {frolics && frolics.length > 0 && (
          <section>
            <div className="container">
            <h1 class="label">Past and Upcoming Frolics</h1>
              <div className="row">
                {frolics.map((props, idx) => (
                  <Frolic key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </div>
    </Layout>
  );
}
